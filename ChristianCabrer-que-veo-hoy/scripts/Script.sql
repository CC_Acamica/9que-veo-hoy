CREATE DATABASE que_veo_hoy;
USE que_veo_hoy;

CREATE TABLE pelicula(
id INT auto_increment primary KEY,
titulo varchar(100),
duracion int(5),
director varchar(400),
anio int(5),
fecha_lanzamiento date,
puntuacion int(2),
poster varchar(300),
trama varchar(700));

ALTER TABLE pelicula ADD genero_id int;

CREATE TABLE genero(
id INT auto_increment primary KEY,
nombre varchar(30));

CREATE TABLE actor(
id INT auto_increment primary KEY,
nombre varchar(70));

CREATE TABLE actor_pelicula(
id INT auto_increment primary KEY,
actor_id int,
pelicula_id int);

ALTER TABLE pelicula ADD trailer varchar(500);