var conexion = require('../lib/conexionbd');

function Obtener_Peliculas(req, res) {
    var Parametros = Obtener_Parametros(req);
    var Consultas = Armado_Consultas_Peliculas(Parametros);
    conexion.query(Consultas.Consulta_Total, function (error, data, fields) {
        if (error) {
            console.log("Se produjo un error al obtener el listado de peliculas. " + error);
            return res.status(404).send("Se produjo un error al obtener el listado de peliculas. ");
        }
        else if (data.length == 0) {
            console.log("Aún no hay peliculas cargadas con los filtros seleccionados.");
            return res.status(404).send("Aún no hay peliculas cargadas con los filtros seleccionados.");
        }
        else {
            var total = data[0].Total;
            conexion.query(Consultas.Consulta_Peliculas, function (error, data, fields) {
                if (error) {
                    console.log("Se produjo un error al obtener el listado de peliculas. " + error);
                    return res.status(404).send("Se produjo un error al obtener el listado de peliculas. ");
                }
                else {
                    var Peliculas = { peliculas: data, total: total };
                    res.send(Peliculas);
                };
            });
        };
    });
};

function Obtener_Peliculas_Recomendadas(req, res) {
    var Parametros = Obtener_Parametros(req);
    var Consulta = Armado_Consulta_Peliculas_Recomendadas(Parametros);
    conexion.query(Consulta, function (error, data, fields) {
        if (error) {
            console.log("Se produjo un error al obtener el listado de peliculas recomendadas. " + error);
            return res.status(404).send("Se produjo un error al obtener el listado de peliculas recomendadas. ");
        }
        else {
            var Peliculas = { peliculas: data };
            res.send(Peliculas);
        };
    });
};

function Obtener_Pelicula_Por_ID(req, res) {
    var id = req.params.id;
    var consulta_pelicula = 'SELECT * FROM que_veo_hoy.pelicula WHERE id = ' + id;
    var consulta_actores = 'SELECT AC.nombre FROM que_veo_hoy.actor_pelicula AS AP INNER JOIN que_veo_hoy.actor AS AC ON AP.actor_id = AC.id WHERE AP.pelicula_id = ' + id;
    var consulta_genero = 'SELECT G.nombre FROM que_veo_hoy.pelicula as P INNER JOIN que_veo_hoy.genero as G ON P.genero_id = G.id WHERE P.id = ' + id;

    conexion.query(consulta_pelicula, function (error, data, fields) {
        if (error) {
            console.log("Se produjo un error al obtener los datos de las pelicula. " + error);
            return res.status(404).send("Se produjo un error al obtener los datos de las pelicula. ");
        }
        else if (data.length == 0) {
            console.log("La pelicula ingresada no existe.");
            return res.status(404).send("La pelicula ingresada no existe.");
        }
        else {
            var Datos_Pelicula = data[0];
            conexion.query(consulta_actores, function (error, data, fields) {
                if (error) {
                    console.log("Se produjo un error al obtener los datos de los actores de pelicula. " + error);
                    return res.status(404).send("Se produjo un error al obtener los datos de los actores de pelicula. " + error);
                }
                else {
                    var Datos_Actores = [];
                    if (data.length > 0) {
                        Datos_Actores = data;
                    }
                    conexion.query(consulta_genero, function (error, data, fields) {
                        if (error) {
                            console.log("Se produjo un error al obtener el genero de la pelicula. " + error);
                            return res.status(404).send("Se produjo un error al obtener el genero de la pelicula. " + error);
                        }
                        else {
                            var Datos_Generos = "";
                            if (data.length > 0) {
                                Datos_Generos = data[0].nombre;
                            }
                            var response = { pelicula: Datos_Pelicula, actores: Datos_Actores, genero: Datos_Generos };
                            res.send(JSON.stringify(response));
                        };
                    });
                };
            });
        };
    });
};

function Obtener_Generos(req, res) {
    var consulta = 'SELECT * FROM que_veo_hoy.genero';
    conexion.query(consulta, function (error, data, fields) {
        if (error) {
            console.log("Se produjo un error al obtener el listado de generos. " + error);
            return res.status(404).send("Se produjo un error al obtener el listado de generos. ");
        }
        else if (data.length == 0) {
            console.log("Aún no hay géneros cargados.");
            return res.status(404).send("Aún no hay géneros cargados.");
        }
        else{
            var generos = { generos: data };
            res.send(generos);
        };
    });
};

function Obtener_Parametros(P_req) {
    var anio = P_req.query.anio;
    var titulo = P_req.query.titulo;
    var genero = P_req.query.genero;
    var columna_orden = P_req.query.columna_orden;
    var pagina = P_req.query.pagina;
    var cantidad = P_req.query.cantidad;
    var anio_inicio = P_req.query.anio_inicio;
    var anio_fin = P_req.query.anio_fin;
    var puntuacion = P_req.query.puntuacion;
    var Parametros = { anio, titulo, genero, columna_orden, pagina, cantidad, anio_inicio, anio_fin, puntuacion };
    return Parametros;
};

function Armado_Consultas_Peliculas(P_Parametros) {
    var Consulta_Peliculas = 'SELECT * FROM que_veo_hoy.pelicula ';
    var flag = true;
    if (P_Parametros.anio) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += 'anio = ' + P_Parametros.anio;
    };
    if (P_Parametros.titulo) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += "titulo like '%" + P_Parametros.titulo + "%'";
    };
    if (P_Parametros.genero) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += 'genero_id = ' + P_Parametros.genero;
    };
    if (P_Parametros.columna_orden) {
        Consulta_Peliculas += ' order by ' + P_Parametros.columna_orden;
    };
    var Consulta_Total = Consulta_Peliculas;
    Consulta_Total = Consulta_Total.replace('*', 'COUNT(*) as Total');

    var Limit_Inf = 0;
    if (P_Parametros.pagina > 1) {
        var Limit_Inf = (P_Parametros.pagina - 1) * P_Parametros.cantidad;
    };
    Consulta_Peliculas += ' LIMIT ' + Limit_Inf + ' ,' + P_Parametros.cantidad;
    console.log(Consulta_Peliculas);
    console.log(Consulta_Total);
    return { Consulta_Peliculas, Consulta_Total };
};

function Armado_Consulta_Peliculas_Recomendadas(P_Parametros) {
    var Consulta_Peliculas = 'SELECT P.*, G.nombre FROM que_veo_hoy.pelicula as P INNER JOIN que_veo_hoy.genero as G on G.id = P.genero_id ';
    var flag = true;
    if (P_Parametros.genero) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += "G.nombre = '" + P_Parametros.genero + "'";
    };
    if (P_Parametros.puntuacion) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += 'P.puntuacion > ' + P_Parametros.puntuacion;
    };
    if (P_Parametros.anio_inicio && P_Parametros.anio_fin) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += "P.anio BETWEEN " + P_Parametros.anio_inicio + " AND " + P_Parametros.anio_fin;
    }
    else if (P_Parametros.anio_inicio) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += "P.anio > " + P_Parametros.anio_inicio;
    }
    else if (P_Parametros.anio_fin) {
        if (flag) {
            Consulta_Peliculas += 'WHERE '
            flag = false;
        }
        else {
            Consulta_Peliculas += ' AND '
        };
        Consulta_Peliculas += "P.anio < " + P_Parametros.anio_fin;
    };
    console.log(Consulta_Peliculas);
    return Consulta_Peliculas;
};

module.exports = { Obtener_Peliculas, Obtener_Peliculas_Recomendadas, Obtener_Pelicula_Por_ID, Obtener_Generos };