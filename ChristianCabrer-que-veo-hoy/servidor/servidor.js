//paquetes necesarios para el proyecto
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var controlador = require('./controladores/Controlador');

var app = express();

app.use(cors());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());
app.get('/peliculas', controlador.Obtener_Peliculas);
app.get('/peliculas/recomendacion', controlador.Obtener_Peliculas_Recomendadas);
app.get('/peliculas/:id', controlador.Obtener_Pelicula_Por_ID);
app.get('/generos', controlador.Obtener_Generos);
//seteamos el puerto en el cual va a escuchar los pedidos la aplicación
var puerto = '8080';

app.listen(puerto, function () {
  console.log( "Escuchando en el puerto " + puerto );
});